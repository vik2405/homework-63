import React, { Component } from 'react';
import './AppTodo.css';
import NewTask from "../../components/Todo/NewTask/NewTask";
import Tasks from "../../components/Todo/Tasks/Tasks";
import axios from 'axios';


class AppTodo extends Component {
    state = {
        addtasktext: '',
        task: [
             {task:' WELCOME!', id: ''}
        ]
    };

    GetTask = () => {
        axios.get('/task.json').then(response => {

            let task = Object.keys(response.data).map(key => {
                return {...response.data[key], id: key};
            });
            this.setState({task});
        });
    };

    componentDidMount() {
        this.GetTask()
            };


    fullchange = (event) => {
        let addtasktext = event.target.value;
        this.setState({addtasktext})
    };
    addTask = () => {
        axios.post('/task.json', {task: this.state.addtasktext}).then(() => {
                this.GetTask();
            }
        )
    };

    removeTask = (id) => {
            axios.delete('/task/' + id + '.json').then(response => {
                this.GetTask()
            });
        };






  render() {
    return (
      <div className="App">
          <h3>TASK FOR TODAY</h3>
       <NewTask change = {this.fullchange} add = {this.addTask } task = {this.state.addtasktext}  />
          <Tasks tasks = {this.state.task} remove = {this.removeTask}/>
      </div>
    );
  }
}

export default AppTodo;
