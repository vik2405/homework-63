import React, { Component } from 'react';
import './AppMovies.css';
import axios from "axios/index";
import NewTask1 from "../../components/Movies/NewTask1/NewTask1";
import Tasks1 from "../../components/Movies/Tasks1/Tasks1";
import Spinner from "../../components/UI/Spinner/Spinner";



class AppMovies extends Component {
    state = {
        addtasktext: ' ',

        task: [
             {task:'New films : ', id: ' '}
        ],
        loading: true
    };

    GetMovie = () => {
        axios.get('/movie.json').then(response => {

            let task = Object.keys(response.data).map(key => {
                return {...response.data[key], id: key};
            });
            this.setState({task});
        });
    };


    fullchange = (event) => {
        let addtasktext = event.target.value;
        this.setState({addtasktext})
    };


    componentDidMount() {
        this.GetMovie()
    };

    addTask = () => {
        axios.post('/movie.json', {task: this.state.addtasktext}).then(() => {
                this.GetMovie();
            }
        )

    };

    removeTask = (id) => {
        axios.delete('/movie/' + id + '.json').then(response => {
            this.GetMovie()
        });
    };


    changeTask = (event, id) => {
        let tasks = [...this.state.task];
        let index = tasks.findIndex(task => task.id === id);
        let task = {...tasks[index]};
        task.task = event.target.value;
        tasks[index] = task;
        this.setState({task: tasks});
    };

    changeTaskPost = (id) => {
        let tasks = this.state.task;
        let index = tasks.findIndex(task => task.id === id);
        console.log(this.state.task[index]);
        axios.patch('/movie/' + id + '.json', this.state.task[index])

    };



  render() {
      if (this.state.loading) {
          <Spinner/>;
      }
    return (
      <div className="App">
          <h3>IT'S IMPOSSIBLE NOT TO LOOK!</h3>
          <h4 className="newmovie">Add new movie</h4>
       <NewTask1 change = {this.fullchange} add = {this.addTask } task = {this.state.addtasktext}  />
          <Tasks1 tasks = {this.state.task} remove = {this.removeTask} change={this.changeTask} changeName={this.changeTaskPost}/>

      </div>
    );
  }
}

export default AppMovies;


