import React from 'react';
import './Tasks1.css';
import Task1 from "./Task1";
let Tasks1 = (props)=> {
    return(
        <div className="tasks">
            {props.tasks.map((task) => <Task1 text={task.task} remove={() => props.remove(task.id)} itemId={task.id} key={task.id} change={(event) => props.change(event, task.id)} changeName={() => props.changeName(task.id)}/>)}

        </div>
    )
};
export default Tasks1;