import React from "react";
import './NewTask.css';
let NewTask = (props)=> {
    return(
        <div className="newtask">
            <textarea className='area' placeholder="Add new task" cols="60" rows="2" value={props.task} onChange={props.change}></textarea>
            <button className="add" onClick={props.add}>Add task</button>
        </div>
    )
};
   export default NewTask;











