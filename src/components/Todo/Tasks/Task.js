import React from 'react';
import './Task.css';
let Task = (props)=> {
    return(
        <div className="task">
            <p className="ptask">{props.text}<span onClick={props.remove}><button className="button">X</button></span></p>
        </div>
    )
};
       export default Task
