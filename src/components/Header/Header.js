import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';
const Header = () => {
    return (
        <header>
            <div className="Navigation">
            <nav className="nav">
                <ul className="ul">
                    <li><NavLink to="/" exact activeClassName="active">Home</NavLink></li>
                    <li><NavLink to="/AppTodo" activeClassName="active">AppTodo</NavLink></li>
                    <li><NavLink to="/AppMovies" activeClassName="active">AppMovies</NavLink></li>
                </ul>
            </nav>
            </div>
        </header>
    )
};

export default Header;