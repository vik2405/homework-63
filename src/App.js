import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Header from "./components/Header/Header";
import Home from "./containers/Home/Home";
import AppMovies from "./containers/AppMovies/AppMovies";
import AppTodo from "./containers/AppTodo/AppTodo";



class App extends Component {
    render() {
        return (
            <div className="App">
                <Header/>
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/AppTodo" component={AppTodo}/>
                    <Route path="/AppMovies" component={AppMovies}/>
                </Switch>
            </div>
        );
    }
}

export default App;